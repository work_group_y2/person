/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.person;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestPerson {

    public static void main(String[] args) {
        Person person1 = new Person("Boom", 21, 0);
        Person person2 = new Person("Viw", 25, 0);
        
        System.out.println(person1);
        person1.box('R');
        System.out.println(person1);
        person1.box('Y');
        System.out.println(person1);
        person1.box('B');
        System.out.println(person1);
        person2.box('R', 2);
        System.out.println(person2);
        person2.box('B', 10);
        System.out.println(person2);
        person1.box(8);
        System.out.println(person1);

    }
}
