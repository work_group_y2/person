/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.person;

/**
 *
 * @author ทักช์ติโชค
 */
public class Person {

    private String name;
    private int age;
    private int num;
    private char checkColor = ' ';

    public Person(String name, int age, int num) { //ตัวที่เป็นบุคคลที่ลงจับฉลาก
        this.name = name;
        this.age = age;
        this.num = num;

    }

    public boolean box(char color) {  //เลือกกล่องสี 'R','G','Y','B' จับแบบที่ละลูก
        switch (color) {
            case 'R':
                num += 1;
                break;
            case 'G':
                num += 1;
                break;
            case 'Y':
                num += 1;
                break;
            case 'B':
                num += 1;
                break;
        }
        checkColor = color;
        return true;
    }

    public boolean box(char color, int num) { //เลือกกล่องสี 'R','G','Y','B' จับแบบจำนวนที่อยากจับ
        for (int i = 0; i < num; i++) {
            if (!this.box(color)) {
                return false;
            }
        }

        return true;
    }

    public boolean box() { //จับแบบเดิมที่ละลูก เลือกสีที่เคยเลือก
        return this.box(checkColor);
    }

    public boolean box(int time) { //จับแบบเดิมจำนวนลูก เลือกสีที่เคยเลือก
        return this.box(checkColor, time);
    }

    public String toString() { //แสดง ชื่อ และ อายุ จำนวนจับ คนลง
        return ("Name: " + this.name + " Age: " + this.age + " Num: " + this.num);
    }

}
